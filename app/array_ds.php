<?php
    $test_array = array(
        'table1' => array(
            array(
                'lastname', 'firstname', 'middle_name', 'userID', 'password'
            ),
            array(
                'Deamo', 'Sandra', '', '1751053', '1643328'
            )
        ),
        'table2' => array(
            array(
                'Event', 'EventID', 'start_date', 'end_date', 'AdminUserID'
            ),
            array(
                'C3S2E10','3','2009-10-16','2010-05-21','1751053'
            )
        )
    );

    echo('<pre>');
    var_dump($test_array);
    echo('/<pre>');
?>