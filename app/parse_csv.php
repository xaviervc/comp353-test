<?php

// $fileLocation = "../uploads/phpEBA.tmp.csv";


function parseCSV($filePath){

    $csvToArray = file($filePath);
    $tableArray = array();
    $count = 0;

    //pop last element as it is not needed
    array_pop($csvToArray);

    //count how many tables there are and preformat the headings for each table
    foreach($csvToArray as $value){
        if(strpos($value, '+-') !== FALSE){
            $count++;
            $tableArray[] = array(
                $count => array()
            );
        }
    }

    $count = -1;
    //split the tables
    foreach($csvToArray as &$value){
        
        //encounter table divider
        if(strpos($value, '+-') !== FALSE){
            array_shift($csvToArray);
            $count++;
        }
        else if (strpos($value, '|') !== FALSE){
            $value = substr($value, 1);
            $tableArray[$count][] = explode('|', $value);
        }

    }

    foreach($tableArray as &$inner){
        array_shift($inner);
        foreach($inner as &$inner_inner){
            array_pop($inner_inner);
        }
    }

    

    echo '<pre>';
    var_dump($tableArray);
    echo '</pre>';

}


//check to see if the button was clicked
if(isset($_POST['parse-csv'])){
    parseCSV($fileLocation);
}

?>