<?php

function upload_file() {
    try {

        //general file check
        if (
            !isset($_FILES['fileToUpload']['error']) || is_array($_FILES['fileToUpload']['error'])
        ) {
            throw new RuntimeException('Invalid Parameters');
        }

        //check error values
        switch ($_FILES['fileToUpload']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOADE_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');
            default:
                throw new RuntimeException('Unkown erros.');
        }

        //check file size limit
        if ($_FILES['fileToUpload']['size'] > 1000000) {
            throw new RuntimeException('Filesize limit exceeded');
        }

        //try this
        $csvFileType = strtolower(pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION));

        if($csvFileType != 'csv'){
            throw new RuntimeException('Invalid file format');
        }

        //semi unique name needs to be reworked
        if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], sprintf('%s.%s', $_FILES['fileToUpload']['tmp_name'],'csv'))) {

            $fileName = sprintf('%s.%s', $_FILES['fileToUpload']['tmp_name'],'csv');
            echo($fileName);

            return $fileName;

        } else {
            throw new RuntimeException("File move failed");
        }

        
    } catch (RuntimeException $e) {
        echo $e->getMessage();
    }
}

$fileName = upload_file();

function parseCSV($fileName){
    
    $csvToArray = file($fileName);
    $tableArray = array();

    //split the tables
    for($i = 0; $i < $csvToArray.length(); $i++){
        if(strcmp($csvToArray[$i], '+----------------------------------------------+' === 0)){
            //anything after these are headers
            echo $csvToArray[$i+1];
        }
    }

}

?>